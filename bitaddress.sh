#!/bin/bash
# ---------------------------------------------------------------------
# Application   · Bitcoin Address 
# Script        · bitaddress.sh
# Description   · Generate private/public bitcoin addresses from a brain input. 
# Version       · 21.10
# License       · GNU/GPL v3.0
# ---------------------------------------------------------------------
# Copyright (C) 2021  
# 
# Contributors:
# 
#  → Paulo C. Ormonde <contado@trameestruturas.com.br>
#  → Tiago Salem <https://tribocrypto.com/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

# Tested on Debian GNU/Linux 11 Bullseye
#
# Possible dependencies:
#
# → sudo apt install openssl
# → sudo apt install base58

# Addresses tests 
# http://gobittest.appspot.com/Address
#
# Note that since the input is a string, the xxd -r -p
# will convert the hex string into binary and then output it in hexdump style (ascii),
# which is what the openssl hashing functions expect as input.

clear
version=80   #EF test  / 80 main
OLDIFS=$IFS
echo " Bitcoin Address"
echo " ---------------"
echo " "
IFS= read -p " Enter the seed: " seed
hash=$(echo -n "$seed" | sha256sum )
private=$(echo ${hash:0:64})
privatekey=$(echo ${private^^})
echo " "
echo "  Private key"
echo "  " $privatekey
hash=$(echo $version$privatekey | xxd -r -p | openssl sha256) 
sha1=$(echo ${hash:9:80})
hash=$(echo $sha1 | xxd -r -p | openssl sha256) 
sha2=$(echo ${hash:9:80})
checksum=$(echo ${sha2:0:8})
wifbase=$version$privatekey$checksum
wif58=$(echo $wifbase | xxd -p -r | base58 )
echo " "
echo " WIF base"
echo "  " $wifbase
echo " "
echo " WIF base58"
echo "  " $wif58

echo " "
echo " Public key"
publickey=$(openssl ec -inform DER -text -noout -in <(cat <(echo -n "302e0201010420") <(echo -n $privatekey) <(echo -n "a00706052b8104000a") | xxd -r -p) 2>/dev/null | tail -6 | head -5 | sed 's/[ :]//g' | tr -d '\n' && echo)
echo "  " $publickey

#Compress bitcoin public key 
hash=$(echo $publickey | xxd -r -p | openssl sha256)
sha1=$(echo ${hash:9:80})
rip=$(echo $sha1 | xxd -r -p | openssl ripemd160 )
ripsha=$(echo ${rip:9:40})
netid=00

#.............checksum
hash=$(echo $netid$ripsha | xxd -r -p | openssl sha256) 
sha1=$(echo ${hash:9:80})
hash=$(echo $sha1 | xxd -r -p | openssl  sha256) 
sha2=$(echo ${hash:9:80})
checksum=$(echo ${sha2:0:8})

btcaddress=$(echo $netid$ripsha$checksum | xxd -r -p | base58 )
echo " "
echo " Bitcoin Public Address - Main Network"
echo "  " $btcaddress 
IFS=$OLDIFS
