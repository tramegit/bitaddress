# BIT ADDRESS WITH SHELL SCRIPT
### Generate private/public bitcoin addresses from a brain input

> **Tested on Debian GNU/Linux 11 Bullseye**


> Possible dependencies:

> → sudo apt install openssl  
> → sudo apt install base58
> → sudo apt install qrencode

> [Address testing suite](http://gobittest.appspot.com/Address)


## Basic usage
→ Prefer to use an offline computer to generate your keys.

→ Open the terminal and Type **. bitaddress.sh**

→ Enter the seed phrase or collection of words in a format that you will can remember.

>Optionally, you can use the **testaddress.sh** for the test network
##### Pay Attention: Never use simple phrases or few words.

![](123456.png)


## Generate QRCODE image
To convert a Public Bitcoin address to QRCODE image you need to have installed in your GNU/Linux OS the qrencode program. 
> Example:
> 
> qrencode -o myqrcode.png 1565qkBbLcuFP78f7MFKkK8jtHGDwdSgvX

![](qrcode.png)

## License


[GPLv3](http://www.gnu.org/licenses/)

[Click here to read the license](license.txt)
